# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
AnnotationsDemo::Application.config.secret_key_base = 'a5dc4d9056ecff184c436c449ffc1fa98f92b7c0ef172d19bc352d6a2d1359f0d2a0842da4970f4839ef69b0f9f69533a317f14c416de648f229af569147599b'
