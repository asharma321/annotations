class RelationshipDefine < ActiveRecord::Migration
  def change
    change_table :annotations do |t|
      t.references :image
    end
  end
end
