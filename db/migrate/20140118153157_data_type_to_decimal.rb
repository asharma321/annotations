class DataTypeToDecimal < ActiveRecord::Migration
  def change
     change_column :annotations, :x_position, :decimal, precision: 5, scale: 4
     change_column :annotations, :y_position, :decimal, precision: 5, scale: 4
     change_column :annotations, :width, :decimal, precision: 5, scale: 4
     change_column :annotations, :height, :decimal, precision: 5, scale: 4
  end
end
