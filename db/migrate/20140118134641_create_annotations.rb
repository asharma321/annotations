class CreateAnnotations < ActiveRecord::Migration
  def change
    create_table :annotations do |t|
      t.string :image
      t.string :text
      t.string :type
      t.integer :x_position
      t.integer :y_position
      t.integer :width
      t.integer :height

      t.timestamps
    end
  end
end
