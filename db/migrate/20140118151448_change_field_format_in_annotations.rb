class ChangeFieldFormatInAnnotations < ActiveRecord::Migration
  def change
    def self.up
     change_column :annotations, :x_position, :decimal
     change_column :annotations, :y_position, :decimal
     change_column :annotations, :width, :decimal
     change_column :annotations, :height, :decimal
    end
    def self.down
      
    end
  end
end
