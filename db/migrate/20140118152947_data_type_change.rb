class DataTypeChange < ActiveRecord::Migration
  def change
    change_column :annotations, :x_position, :decimal
    change_column :annotations, :y_position, :decimal
    change_column :annotations, :width, :decimal
    change_column :annotations, :height, :decimal
  end
end
