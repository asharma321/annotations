class FieldDataTypeChangeToDecimal < ActiveRecord::Migration
  def change
    def self.up
     change_column :annotations, :x_position, :decimal, precision: 5, scale: 2
     change_column :annotations, :y_position, :decimal, precision: 5, scale: 2
     change_column :annotations, :width, :decimal, precision: 5, scale: 2
     change_column :annotations, :height, :decimal, precision: 5, scale: 2
    end
  end
end
