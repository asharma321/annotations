class RenameTypetoImageType < ActiveRecord::Migration
  def change
    rename_column :annotations, :type, :imagetype
  end
end
