class RenameImagetoImageName < ActiveRecord::Migration
  def change
    rename_column :annotations, :image, :image_name
  end
end
