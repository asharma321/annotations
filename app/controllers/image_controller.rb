class ImageController < ApplicationController
 # post /images
  def create
    @image = Image.create(image_params)
    if @image.id.present?
      redirect_to "/images/new" ,  notice: "image uploaded successfully."
    else
      redirect_to "/images/new" ,  notice: "image upload failed." 
    end
  end

  def index
    @images = Image.all
  end

  def new
    @image = Image.new
  end
 
  def show
    @image = Image.find(params[:id])
  end

  private
 
  def image_params
    params.require(:image).permit(:avatar)
  end

end
