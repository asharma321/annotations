class AnnotationsController < ApplicationController
  #before_action :set_annotation, only: [:show, :edit, :update, :destroy]

  # GET /annotations
  # GET /annotations.json
  def show
    render json: Image.find(params[:id]).annotations
  end

  # POST /annotations
  # POST /annotations.json
  def create
    text = params[:data][:text]
    image_name = params[:data][:image_name]
    image_type = params[:data][:image_type]
    x_position = params[:data][:x_position]
    y_position = params[:data][:y_position]
    width = params[:data][:width]
    height = params[:data][:height]
    @image = Image.find(params[:data][:image_id])
    id = @image.annotations.create(:text => text , :image_name => image_name , :imagetype => image_type , :x_position => x_position , :y_position => y_position , :width => width , :height => height).id
    render json: id
  end
  
  # delete /annotations/:id
  def destroy
    Annotation.delete(params[:id])
    render json: "Image deleted successfully."
  end
  
  # put /annotations/:id
  def update
    @annotation = Annotation.find(params[:id])
    @annotation.text = params[:text]
    @annotation.save
    render json: "Image update successfully."
  end
  
end
