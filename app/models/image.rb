class Image < ActiveRecord::Base
   has_attached_file :avatar, :styles => { :medium => "700x700>", :thumb => "50x50>" }
   has_many :annotations
end
