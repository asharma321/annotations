function init(){
  image_id = jQuery('#image_id').val();
  jQuery.ajax({
    type : "get",
   	url : '/annotations/' + image_id,
   	success : function(data){
   		        if(data.length == 0){
   		          alert('No saved Annotation. Create one');	
   		        }
                for(i = 0 ; i < data.length ; i++){	
                  make_annotations(data[i]);
                }    	
              }
  });
};
                
function make_annotations(data){
  x_pos = parseFloat(data.x_position);
  y_pos = parseFloat(data.y_position);
  wid = parseFloat(data.width);
  ht = parseFloat(data.height);
  annotation = { 	
    src : data.image_name,
    text: data.text,
    editable : false,
    shapes : [{
      type : 'rect',
      geometry : { x : x_pos , y : y_pos , width : wid  , height : ht  }
    }]
  };
  annotation.id = data.id;
  anno.addAnnotation(
  	annotation
  );   	
}               
                
anno.addHandler('onAnnotationCreated', function(annotation) {
  
  var image_name = annotation.src;
  var text = annotation.text;
  var image_type = annotation.shapes[0].type;
  var x_position = annotation.shapes[0].geometry.x;
  var y_position = annotation.shapes[0].geometry.y;
  var width = annotation.shapes[0].geometry.width;
  var height = annotation.shapes[0].geometry.height;
  var image_id = jQuery('#image_id').val();
  var image_data = { 
  	                 data : {
  	                   "text" : text , 
  	                   "image_name" : image_name,
  	                   "image_type" : image_type , 
  	                   "x_position" : x_position , 
  	                   "y_position" : y_position , 
  	                   "width" : width , 
  	                   "height" : height,
  	                   "image_id" : image_id 
  	                 }
  	               };
  
  post("/annotations",image_data,annotation);  	   
 });
 
anno.addHandler('onAnnotationRemoved', function(annotation) {
  jQuery.ajax({
    type: "DELETE",
  	url: "/annotations/" + annotation.id
  });
});

anno.addHandler('onAnnotationUpdated', function(annotation) {
  jQuery.ajax({
  	type : "PUT",
  	data : { "text" : annotation.text },
  	url : "/annotations/" + annotation.id
  });	
});
 
function post(url,data,annotation){
  jQuery.ajax({
     type: "POST",
	 url: url,
     data: data,
     success : function(data){
       annotation.id = data;
     }
  });
}

